consoleSome();
// appendAPic();
var html = document.querySelector('html');
if (html) {
    setEleSelectAuto(html);
    var iframes = html.querySelectorAll('iframe');
    if (iframes && iframes.length) {
        for (var i = 0; i < iframes.length; i++) {
            var f = iframes[i];
            f.contentDocument && (
                f.contentDocument.addEventListener('mousedown', sendText)
            )
        }
    }
}
var body = document.querySelector('body');
var allEle = [];
if (body) {
    allEle = body.querySelectorAll('*');
} else {
    allEle = document.querySelectorAll('*');
}
for (var i = 0; i < allEle.length; i++) {
    var e = allEle[i];
    setEleSelectAuto(e);
    e.addEventListener('copy',
        function (e) {
            if (!coping) {
                e.preventDefault();
                e.stopPropagation();
                var d = e.clipboardData || window.ClipboardData;
                if (d) {
                    var s = window.getSelection().toString();
                    console.log(s);
                    myCopy(s);
                }
            }
            setTimeout(function(){coping = false},500);
        }
    );

}
var coping = false;
document.onbeforecopy = function (e) {
    coping = true;
}

var sendText = function (e) {
    if (e.button == 2) {
        e.preventDefault();
        e.stopPropagation();
        if (e.target) {
            try {
                var html = null, text = null;
                if (e.target.tagName == 'INPUT' || e.target.tagName == 'TEXTAREA') {
                    html = e.target.value;
                    text = e.target.value;
                } else {
                    html = e.target.innerHTML;
                    text = e.target.innerText;
                }
                chrome.runtime.sendMessage({
                    from: 'webUsage.js',
                    content:
                    {
                        html: html,
                        text: text
                    }
                }, function (msg) {
                    console.log(msg);
                });
            } catch (error) {
                console.error(error);
            }

        }
    }
}

document.addEventListener('mousedown', sendText);








