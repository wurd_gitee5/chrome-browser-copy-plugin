var randRgb = function () {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    return `rgb(${r},${g},${b})`;
}
var consoleSome = function () {
    var randBack = function () {
        return 'background:' + randRgb();
    }
    var randColor = function () {
        return 'color:' + randRgb();
    }
    var consoleColor = function () {
        console.log('%c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c ' +
            '%c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c ',
            randBack(), randBack(), randBack(), randBack(), randBack(), randBack(),
            randBack(), randBack(), randBack(), randBack(), randBack(), randBack(),
            randBack(), randBack(), randBack(), randBack(), randBack(), randBack(),
            randBack(), randBack(), randBack(), randBack(), randBack(), randBack(),
            randBack(), randBack(), randBack(), randBack(), randBack(), randBack(),
            randBack(), randBack(), randBack(), randBack(), randBack(), randBack(),
            randBack(), randBack()
        );
    }
    consoleColor();
    console.log('%c                  無                  ', randColor());
    console.log('%c                  **                  ', randColor());
    console.log('%c                 &**&                 ', randColor());
    console.log('%c                ^&**&^                ', randColor());
    console.log('%c               %^&**&^%               ', randColor());
    console.log('%c              $%^&**&^%$              ', randColor());
    console.log('%c             #$%^&**&^%$#             ', randColor());
    console.log('%c            @#$%^&**&^%$#@            ', randColor());
    console.log('%c           !@#$%^&**&^%$#@!           ', randColor());
    console.log('%c          ~!@#$%^&**&^%$#@!~          ', randColor());
    console.log('%c~~~~~~~~~~~!@#$%^&**&^%$#@!~~~~~~~~~~~', randColor());
    console.log('%c          ~!@#$%^&**&^%$#@!~          ', randColor());
    console.log('%c           !@#$%^&**&^%$#@!           ', randColor());
    console.log('%c            @#$%^&**&^%$#@            ', randColor());
    console.log('%c             #$%^&**&^%$#             ', randColor());
    console.log('%c              $%^&**&^%$              ', randColor());
    console.log('%c               %^&**&^%               ', randColor());
    console.log('%c                ^&**&^                ', randColor());
    console.log('%c                 &**&                 ', randColor());
    console.log('%c                  **                  ', randColor());
    console.log('%c                  無                  ', randColor());
    consoleColor();
}
var appendAPic = function () {
    imgEle = document.createElement('img');
    imgEle.src = chrome.runtime.getURL("icon.png");
    imgEle.style = 'display: block;margin: 0 auto;';
    document.getElementsByTagName('body')[0].appendChild(imgEle)

    imgEle = document.createElement('div');
    imgEle.style = 'height:44px;';
    document.getElementsByTagName('body')[0].appendChild(imgEle)
}
var setEleSelectAuto = function (ele) {
    ele.style['user-select'] = 'auto';
    ele.style['-webkit-touch-callout'] = 'auto';
    ele.style['-webkit-user-select'] = 'auto';
    ele.style['-khtml-user-select'] = 'auto';
    ele.style['-moz-user-select'] = 'auto';
    ele.style['-ms-user-select'] = 'auto';
}
var myCopy = function (str) {
    var input = document.createElement('textarea');
    input.value = str;
    document.body.appendChild(input);
    input.select();
    document.execCommand('Copy');
    document.body.removeChild(input);
}
