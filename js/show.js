window.onblur = function () {
    chrome.tabs.getCurrent(e => chrome.tabs.remove(e.id))
}

chrome.runtime.sendMessage({
    from: 'show.html'
}, function (data) {
    console.log(data);
    if (data) {
        document.getElementById('show_html').value = data.html;
        document.getElementById('show_text').value = data.text;
    }
});
