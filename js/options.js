
var ocrSelect = document.getElementById('ocr-select');
var ocrSelectType = localStorage.getItem('ocr-select');
console.log('current ocr: ', ocrSelectType);
if (ocrSelectType) {
    for (var i = 0; i < ocrSelect.options.length; i++) {
        if (ocrSelect.options[i].value == ocrSelectType) {
            ocrSelect.options[i].selected = true;
            break;
        }
    }
}
ocrSelect.onchange = function (e) {
    localStorage.setItem('ocr-select', e.target.value);
}